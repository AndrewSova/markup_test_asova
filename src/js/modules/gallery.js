// This is an examples of simple export.
//
// You can remove or add your own function in this file.

var NEWGALLERY = {
  galleryFunc: function() {

    $.fn.gallery = function(settings) {
        var self = $(this);

        if (self.length > 0) {
            var optionGallery = {
                'effect': 'carousel', // fade, carousel
                'rotate': 'false', // true/false
                'delay': '5000', // 0....
                'countClone': 1
            };

            var buttonsHolder = '<div class="gallery-nav"><button class="btn-prev">&lt;</button><button class="btn-next">&gt;</button></div>',
                slides = self.find('.holder ul li'),
                countSlides = slides.length,
                holderSlides = self.find('.holder ul'),
                rotateInterval,
                countClone = optionGallery.countClone;

            if(settings) {
                var optionsGalleryJson = settings;

                for (var prop in optionsGalleryJson) {
                    optionGallery[prop] = optionsGalleryJson[prop];
                }

            }

            /* add buttons on DOM */
            self.append(buttonsHolder);

            /* change sliders for click on Buttons */
            var btnPrev = self.find('.btn-prev'),
                btnNext = self.find('.btn-next');

            /* add .active for First slide */
            slides.eq(0).addClass('active');

            /* add Switcher */
            addSwitcher();

            /* slide to Prev/Next */
            prevSlide();
            nextSlide();

            /* add active Switcher */
            activeSwitcherItem();

            /* click to Switcher, scroll to Active */
            clickSwither();

            /* rotate gallery */
            if (optionGallery.rotate == 'true') {
                self.hover(
                    function(){
                        stopRotateGallery();
                    },
                    function(){
                        rotateGallery(optionGallery.delay);
                    });
                //rotateGallery(optionGallery.delay);
            } else {
                stopRotateGallery();
            }

            if (optionGallery.effect == 'carousel') {
                /* add width to Holder of gallery */
                widthHolderGallery();
                scrollToActive();
                cloneItems();

                $(window).resize(function () {
                    widthHolderGallery();
                    setTimeout(function () {
                        scrollToActive();
                    }, 1000);
                });

            }

            swipeFade();

            /* clone gallery items */
            function cloneItems() {
                var cloneEnd = {},
                    cloneStart = {};
                if (countClone < slides.length) {
                    for (var i = 0; i < countClone; i++) {
                        cloneEnd += slides.eq(i).clone().addClass("clone").removeClass('active').removeClass('active').appendTo(holderSlides);
                        cloneStart += slides.eq(slides.length - i - 1).clone().addClass("clone").removeClass('active').prependTo(holderSlides);
                    }
                }

                scrollToActive();
                setTimeout(function () {
                    addAnimation();
                }, 0);
            }

            /* add scroll animation */
            function addAnimation() {
                self.addClass('has-animation');
            }

            /* remove scroll animation */
            function removeAnimation() {
                self.removeClass('has-animation');
            }

            /* gallery size */
            function widthHolderGallery() {
                var slideItemWidth = self.width(),
                    slideWidth = slideItemWidth * countSlides * 2;
                var slidesClone = self.find('.holder ul li');
                slidesClone.width(slideItemWidth);
                holderSlides.width(slideWidth);
            }

            /* choose prev Slide */
            function prevSlide() {
                btnPrev.click(function () {
                    prev();
                });
            }

            /* prev Slide */
            function prev() {
                var slidesClone = self.find('.holder ul li'),
                    slideActive = self.find('.holder ul li.active'),
                    slideActiveIndex = slideActive.index() - countClone;
                slideActive.removeClass('active');
                if (optionGallery.effect == 'carousel') {

                    if (slideActiveIndex >= 1) {
                        slides.eq(slideActiveIndex - 1).addClass('active');
                    } else {
                        slidesClone.eq(0).addClass('active');
                        slideActive = self.find('.holder ul li.active');
                        setTimeout(function () {
                            removeAnimation();
                            slideActive.removeClass('active');
                            slidesClone.eq(slidesClone.length - countClone - 1).addClass('active');
                            scrollToActive();
                            setTimeout(function () {
                                addAnimation();
                            }, 10);
                        }, 250);
                    }
                } else {
                    slideActiveIndex = slideActive.index();
                    if (slideActiveIndex !== 0) {
                        slides.eq(slideActiveIndex - 1).addClass('active');
                    } else {
                        slides.eq(countSlides - 1).addClass('active');
                    }
                }

                scrollToActive();
            }
            /* choose next Slide */
            function nextSlide() {
                btnNext.click(function () {
                    next();
                });
            }

            /* next Slide */
            function next() {
                var slidesClone = self.find('.holder ul li'),
                    slideActive = self.find('.holder ul li.active'),
                    slideActiveIndex = slideActive.index();

                slideActive.removeClass('active');
                if (optionGallery.effect == 'carousel') {
                    if (slideActiveIndex < countSlides) {
                        slidesClone.eq(slideActiveIndex + 1).addClass('active');
                    } else {
                        slidesClone.eq(slideActiveIndex + 1).addClass('active');
                        slideActive = self.find('.holder ul li.active');
                        setTimeout(function () {
                            removeAnimation();
                            slideActive.removeClass('active');
                            slidesClone.eq(1).addClass('active');
                            scrollToActive();
                            setTimeout(function () {
                                addAnimation();
                            }, 5);
                        }, 250);
                    }
                } else {
                    if (slideActiveIndex < countSlides - 1) {
                        slides.eq(slideActiveIndex + 1).addClass('active');
                    } else {
                        slides.eq(0).addClass('active');
                    }
                }

                scrollToActive();
            }

            /* scroll to active slide */
            function scrollToActive() {
                if (optionGallery.effect == 'carousel') {
                    var slideActive = self.find('.holder ul li.active'),
                        slideActiveWidth = slideActive.width(),
                        slideActiveIndex = slideActive.index(),
                        positionActiveSlide = slideActiveIndex * slideActiveWidth;
                    holderSlides.css('margin-left', -positionActiveSlide);
                }

                activeSwitcherItem();
            }

            /* create switcher */
            function createSwitcher(slides) {
                var allCreateSwitchers = '';
                for (var i = 0; i < slides; i++) {
                    allCreateSwitchers += '<li><button>' + (+i + 1) + '</button></li>';
                }
                return allCreateSwitchers;
            }

            /* add switcher */
            function addSwitcher() {
                var switcherList = createSwitcher(countSlides),
                    switcherHolder = '<ul class="swither">' + switcherList + '</ul>';
                self.append(switcherHolder);
            }

            /* add class 'active' on active switcher item */
            function activeSwitcherItem() {
                var slideActive = self.find('.holder ul li.active'),
                    slideActiveIndex = slideActive.index();

                var switcherActive = self.find('.swither li.active'),
                    switcherList = self.find('.swither li');
                if (switcherActive.length) {
                    switcherActive.removeClass('active');
                }
                if (optionGallery.effect == 'carousel') {
                    switcherList.eq(slideActiveIndex - countClone).addClass('active');
                } else {
                    switcherList.eq(slideActiveIndex).addClass('active');
                }
            }
            /* click to Switcher, scroll to actuall item */
            function clickSwither() {
                var switcherButtonList = self.find('.swither button');
                switcherButtonList.click(function () {
                    var clickSelf = $(this),
                        clickHolder = clickSelf.closest('li'),
                        clickElIndex = clickHolder.index();

                    var slideActive = self.find('.holder ul li.active'),
                        slideActiveIndex = slideActive.index();

                    slideActive.removeClass('active');
                    slides.eq(clickElIndex).addClass('active');

                    scrollToActive();
                });
            }

            /* rotate Gallery */
            function rotateGallery(interval) {
                rotateInterval = setInterval(function() {
                    next();
                }, interval);
            }

            /* stop rotate Gallery */
            function stopRotateGallery() {
                clearTimeout(rotateInterval);
            }

            function swipeFade() {

                self.find('.holder').on("swipeleft",function() {
                    next();
                });

                self.find('.holder').on("swiperight",function() {
                    prev();
                });

            }
        }
    };

    $('.carousel').gallery({
        'rotate': 'true'
    });

    $('.fade').gallery({
        'effect': 'fade',
        'rotate': 'true',
        'dalay': '9000'
    });

  }
};

export default NEWGALLERY;